# ollnmp

由于lib安装包比较大，保存在百度云上，请到百度云下载  
链接：https://pan.baidu.com/s/1t_H5El7HCIqEdNfeyK4YQg   
提取码：0mel   


相关安装步骤： https://blog.csdn.net/xkjscm/article/details/89297127 

#### 介绍
完全离线安装   centos7 + nginx1.15 + mysql5.7.23 + php7.2 + redis4.0.10 + thinkphp

#### 软件架构

#### 安装教程
（见项目附件文档）
在服务器无法连接外网（内网服务器）情况下，使用脚本一键安装nginx+mysql+PHP+redis+thinkPHP
1. 挂载镜像，请挂载同Linux系统相同版本的号的everything版本，挂载镜像，例如：系统是centos7.2，建议挂载CentOS-7-x86_64-DVD-1511_7.2.iso
2. 上传项目代码到服务器，例如/root/下，解压并进入ollnmp  
    cd /root/   
    tar -zxvf ollnmp.tar.gz  
    cd ./ollnmp  
    ./install.sh lnmp 将安装mysql、redis、nginx、PHP、thinkPHP5、ollnmp  
    （  ./install.sh lnmp|mysql|redis|nginx|php|app|ollnmp|python   ）
3. 安装完毕，执行 ollnmp show  可以查看服务是否正常启动

#### 使用说明

1. ollnmp 是自定命令
2. 其中syn_run 是基于应用的定时器，每秒执行一次，执行地址 /usr/local/nginx/html/sm/shell/app/heat_jump    
  要定时执行某个文件，请在 /usr/local/nginx/html/sm/shell/app/ids_works.sh 中添加其相对路径，并重启服务 ollnmp restart
3. thinkPHP源码地址 在code下面，ollnmp命令在bin下

#### 参与贡献

1. 阿军



